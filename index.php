<?php
ini_set('display_errors', E_ALL);
spl_autoload_extensions('.php');
spl_autoload_register(function ($className) {
    $path = str_replace('\\', '/', $className) . '.php';
    require_once($path);
});

spl_autoload_register();

$kernel = new \base\Kernel();

try {
    print $kernel->response();
} catch (\Exception $e) {
    echo $e->getMessage();
}
