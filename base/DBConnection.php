<?php

namespace base;

/**
 * Class DBConnection
 *
 * Синглтон подключения к БД
 */
class DBConnection
{
    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @return \PDO Подключение к базе данных
     */
    public static function getInstance() {

        // @todo вынести отдельно получение данных из конфига
        $configContents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/config.yml');
        $config = yaml_parse($configContents);

        if (empty($instance)) {
            static::$instance = new \PDO('mysql:host=localhost;dbname=sargemen', $config['DB']['login'], $config['DB']['password']);
        }

        return static::$instance;
    }

    /**
     * DBConnection constructor.
     *
     * Конструктор закрыт
     */
    private function __construct()
    {
    }

    /**
     * Копирование закрыто
     */
    private function __clone()
    {
    }
}
