<h1>Новости</h1>
<?php foreach ($news as $new):?>
    <div class="card mt-3">
        <div class="card-header">
            <?=date('d.m.Y', strtotime($new['created_at']))?>
        </div>
        <div class="card-body">
            <div class="float-right"><a href="/news/delete/?id=<?=$new['id']?>" class="text-danger">Удалить</a></div>
            <h5 class="card-title"><a href="/news/detail/?id=<?=$new['id']?>"><?=$new['title']?></a></h5>
            <p class="card-text"><?=$new['anons']?></p>
        </div>
    </div>
<?php endforeach;?>
<?php if (count($news) === 0):?>
<div class="alert alert-primary" role="alert">
    Новостей пока нет.
</div>
<?php endif;?>

<div class="mt-5 shadow p-3 mb-5 bg-white rounded">
    <h3>Добавить новость</h3>
    <form method="post" action="/news/add/">
        <div class="form-group">
            <label for="title">Заголовок</label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Заголовок новости">
        </div>
        <div class="form-group">
            <label for="anons">Краткий анонс</label>
            <textarea id="anons" class="form-control" name="anons" placeholder="Краткий анонс"></textarea>
            <small class="form-text text-muted">Отображается в списке новостей</small>
        </div>
        <div class="form-group">
            <label for="detail">Подробное описание</label>
            <textarea id="detail" class="form-control" name="detail" placeholder="Подробное описание"></textarea>
            <small class="form-text text-muted">Отображается на странице новости</small>
        </div>
        <button type="submit" class="btn btn-primary">Добавить</button>
    </form>
</div>
