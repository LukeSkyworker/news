<?php

namespace controller;

/**
 * Class NewsController
 *
 * Контроллер ленты новостей.
 */
class NewsController extends BaseController
{
    /**
     * Главная страница ленты новостей: список новостей
     *
     * @return string Response
     */
    public function indexAction()
    {
        // @todo Сделать единую обертку для запросов в БД
        $pdo = \base\DBConnection::getInstance();
        $result = $pdo->query('SELECT * FROM news WHERE deleted_at is NULL');
        $news = $result->fetchAll();

        //@todo вынести сущность новости в модель NewsArticle (extends BaseModel)
        $data = [
            'news' => $news
        ];

        return $this->renderBase('newsList.php', $data);
    }

    /**
     * Страница просмотра/редактирования новости
     *
     * @return string
     * @throws \Exception
     */
    public function detailAction()
    {
        $pdo = \base\DBConnection::getInstance();
        $stmt = $pdo->prepare("SELECT * FROM news WHERE id = :id AND deleted_at IS NULL");
        $stmt->execute([':id' => $_REQUEST['id']]);
        $new = $stmt->fetch();
        if (empty($new)) {
            throw new \Exception('New article id '. $_REQUEST['id']. ' not found!');
        }

        $data = [
            'new' => $new
        ];

        return $this->renderBase('newsDetail.php', $data);
    }

    /**
     * Обновление новости в БД
     *
     * @return string
     * @throws \Exception
     */
    public function updateAction()
    {
        $pdo = \base\DBConnection::getInstance();
        $stmt = $pdo->prepare("
          UPDATE news SET 
            title = :title,
            anons = :anons,
            detail = :detail
          WHERE id = :id
        ");

        $stmt->execute([
            ':id' => $_POST['id'],
            'title' => $_POST['title'],
            'anons' => $_POST['anons'],
            'detail' => $_POST['detail'],
        ]);

        $_SESSION['status'] = 'success';
        $_SESSION['message'] = 'Новость успешно сохранена';

        return $this->detailAction();
    }

    /**
     * Добавление новости в БД
     */
    public function addAction()
    {
        $pdo = \base\DBConnection::getInstance();
        $title = trim($_POST['title']);
        $anons = trim($_POST['anons']);
        $detail = trim($_POST['detail']);

        $pdo->query("
          INSERT INTO news (title, anons, detail) 
          VALUES ('$title', '$anons', '$detail')
        ");

        header('Location: /news/');
    }

    /**
     * Удаление новости по id
     */
    public function deleteAction()
    {
        $pdo = \base\DBConnection::getInstance();
        $stmt = $pdo->prepare('UPDATE news SET deleted_at = CURRENT_TIMESTAMP WHERE id = :id');
        $stmt->execute([':id' => $_GET['id']]);

        header('Location: /news/');
    }
}
