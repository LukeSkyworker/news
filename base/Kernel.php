<?php

namespace base;

/**
 * Class Kernel
 *
 * Ядро
 */
class Kernel
{
    /**
     * @var array Конфиг
     */
    protected $config;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {

    }

    /**
     * Роутинг, вызов нужно контроллера и возврат ответа
     *
     * @return string Response
     */
    public function response()
    {
        // @todo распарсенный yaml закешировать
        $routeContents = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/routes.yml');
        $routes = yaml_parse($routeContents);

        $controllerName = '';
        $actionName = 'indexAction';
        foreach ($routes as $route) {
            if ($_SERVER['SCRIPT_URL'] === $route['path']) {
                $controllerName = '\\controller\\' . $route['controller'] . 'Controller';
                $actionName = $route['action'] . 'Action';

                break;
            }
        }

        if (!empty($controllerName)) {
            $controller = new $controllerName;

            return call_user_func([$controller, $actionName]);
        }

        return '';
    }
}
