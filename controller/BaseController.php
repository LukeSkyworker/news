<?php

namespace controller;

/**
 * Class BaseController
 *
 * Базовый контроллер
 */
class BaseController
{
    /**
     * Рендеринг с базовым лэйаутом
     *
     * @param $templatePath
     * @param $data
     * @return string
     */
    protected function renderBase($templatePath, $data)
    {
        extract($data);
        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/view/' . $templatePath);
        $bodyContent = ob_get_contents();
        ob_end_clean();

        ob_start();
        include($_SERVER['DOCUMENT_ROOT'] . '/view/baseLayout.php');
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }
}
