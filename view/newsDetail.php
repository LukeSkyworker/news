<a href="/news/"><-- Список новостей</a>
<br><br>
<h1>Просмотр новости</h1>
<div class="card mt-3">
    <div class="card-body">
        <div class="float-right"><a href="/news/delete/?id=<?=$new['id']?>" class="text-danger">Удалить</a></div>
        <?=date('d.m.Y', strtotime($new['created_at']))?><br>
        <h5 class="card-title"><?=$new['title']?></h5>
        <p class="card-text"><?=$new['detail']?></p>
    </div>
</div>

<div class="mt-5 shadow p-3 mb-5 bg-white rounded">
    <h3>Редактировать новость</h3>
    <form method="post" action="/news/update/">
        <div class="form-group">
            <label for="title">Заголовок</label>
            <input type="text" value="<?=$new['title']?>" class="form-control" name="title" id="title" placeholder="Заголовок новости">
        </div>
        <div class="form-group">
            <label for="anons">Краткий анонс</label>
            <textarea id="anons" class="form-control" name="anons" placeholder="Краткий анонс"><?=$new['anons']?></textarea>
            <small class="form-text text-muted">Отображается в списке новостей</small>
        </div>
        <div class="form-group">
            <label for="detail">Подробное описание</label>
            <textarea id="detail" class="form-control" name="detail" placeholder="Подробное описание"><?=$new['detail']?></textarea>
            <small class="form-text text-muted">Отображается на странице новости</small>
        </div>

        <input type="hidden" name="id" value="<?=$new['id']?>">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>

<?php if (isset($_SESSION['status']) && $_SESSION['status'] === 'success'):?>
<div class="alert alert-success" role="alert">
    <?=$_SESSION['message']?>
</div>
<?php endif;?>

